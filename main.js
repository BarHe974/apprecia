const { app, BrowserWindow, ipcMain, dialog } = require('electron')
const editJsonFile = require("edit-json-file");
const fs = require('fs');
const pathResolver = require("path")
//var file = editJsonFile(JSON.parse(fs.readFileSync(pathResolver.join( __dirname, "./Test3-Copie.json"), "utf8")));
let file = editJsonFile(pathResolver.join( __dirname, "Test3-Copie.json"));
const EventEmitter = require('events')

const loadingEvents = new EventEmitter()
const createMainWindow = () => new BrowserWindow({
  width: 1000,
  height: 600,
  webPreferences: {
    nodeIntegration: true,
    contextIsolation: false
  }
})

app.on('ready', () => {
  const window = createMainWindow()
  window.loadFile('index.html')
/*
  loadingEvents.on('finished', () => {
  window.loadFile('index.html')
})

  setTimeout(() => loadingEvents.emit('finished'), 3000)*/
})

//app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

ipcMain.on('open-file-dialog', function (event) {
  console.log("open")
  dialog.showOpenDialog({
    properties: ['openFile'],
    filters: [
      { name: 'Table', extensions: ['csv'] }
    ]
  }).then(result => {
    console.log(result.canceled)
    console.log(result.filePaths)
    event.sender.send('selectedElement', result.filePaths, result)
  }).catch(err => {
    console.log(err)
  })
  
})

ipcMain.on('save-file-dialog', function (event) {
  console.log("save")
  dialog.showSaveDialog({
    properties: ['createDirectory', 'showOverwriteConfirmation'],
    filters: [
      { name: 'Table', extensions: ['csv'] },
      { name: 'All Files', extensions: ['*'] }
    ]
  }).then(result => {
    console.log(result.canceled)
    console.log(result.filePath)
    event.sender.send('saveFile', result.filePath, result)
  }).catch(err => {
    console.log(err)
  })
  
})


ipcMain.on('addJson', function (event, app, s, l, t, c, id) {
  console.log(l)
  if(l=="niveau3"){
    file.set(s +"."+l+"."+t+"."+c+"."+id, app);
  }
  else{
    file.set(s +"."+l+"."+t+"."+id, app);
  }
  console.log(file.get());
  file.save();
  event.sender.send("editDic")
})

ipcMain.on('editJson', function (event, app, s, l, t, c, id) {
  
  if(l=="niveau3"){
    file.set(s +"."+l+"."+t+"."+c+"."+id, app);
  }
  else{
    file.set(s +"."+l+"."+t+"."+id, app);
  }
  console.log(file.get());
  file.save();
  event.sender.send("editDic")
})

ipcMain.on('delJson', function (event, s, l, t, c, id) {
  if(l=="niveau3"){
    file.unset(s +"."+l+"."+t+"."+c+"."+id);
  }
  else{
    file.unset(s +"."+l+"."+t+"."+id);
  }
  console.log(s +"."+l+"."+t+"."+id)
  file.save();
  event.sender.send("editDic")
})

ipcMain.on('save-param', function (event, paramFile, paramPos, paramNeg) {
  paramFile = editJsonFile(paramFile);
  paramFile.set("positif.seuil", paramPos);
  paramFile.set("négatif.seuil", paramNeg);
  console.log(paramFile.get());
  paramFile.save();
})
