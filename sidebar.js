document.write('<div class="d-flex flex-column flex-shrink-0 p-3 text-lavenderWeb bg-charcoal h-100 shadow-lg" style="width: 280px;">'+
'<h2 class="display-4 text-center text-lavenderWeb unselectable">APPRECIA</h2>' +
'<hr>' +
'<ul class="nav nav-pills flex-column mb-auto text-center">' +
  '<li class="nav-item my-3">' +
    '<button type="button" id="select" class="btn btn-earthYellow shadow w-100">Importer .csv</button>' +
  '</li>' +
  '<li class="my-3">' +
    '<button type="button" onclick="window.location.href="./dictionnaire3.html" class="btn btn-earthYellow shadow w-100">Dictionnaire</button>'  +
  '</li>' +
  '<li class="my-3">' +
    '<button type="button" onclick="window.location.href="./parametres.html" class="btn btn-earthYellow shadow w-100">Paramètres</button>' +
  '</li>' +
'</ul>' +
'</div>')