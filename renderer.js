const url = require('url');
const editJsonFile = require("edit-json-file");
const createCsvWriter = require('csv-writer').createArrayCsvWriter;
const { ipcRenderer } = require('electron');
const { param } = require('jquery');
const selectBtn = document.getElementById('select')
var downloadBtn = document.getElementById('download')
const btnSave = document.getElementsByName('btnSave')[0]
const pathResolver = require("path")
//const fs = require('fs');
//edit myFile however you want
//fs.writeFileSync('./Test3-Copie.json', JSON.stringify(myFile, null, 4));



if (selectBtn){
  selectBtn.addEventListener('click', function (event) {
    ipcRenderer.send('open-file-dialog')
  })
}


ipcRenderer.on('selectedElement', function (event, path, result) {
  if(result.canceled == false){
    document.getElementById('selected').innerHTML = 'Vous avez sélectionné : ' + path
    console.log(url.pathToFileURL(path.toString()))
    document.getElementById("table").innerHTML = " "
    document.getElementById("modalBody").innerHTML = " "
    importCSV(url.pathToFileURL(path.toString()))
    document.getElementById("start").innerHTML = '<div class="col-auto py-3 mb-3">'
    + '<select id="SelectLevel" class="form-select shadow-sm">'
    + '<option selected>Open this select menu</option></select>'
    + '</div>'
    + '<div class="col-auto py-3 mb-3">'
    + '<button type="button" onclick=generate() class="btn btn-charcoal shadow">Lancer le générateur</button>'
    + '</div>'
    + '<div class="col-auto py-3">'
    + '<button type="button" id="download" class="btn btn-gris shadow">'
    + '<i class="bi bi-download"></i>'
    + '</button>'
    + '</div>'
    + '<div class="col-auto py-3">'
    + '<button type="button" onclick="options()" id="optionNiv3" class="btn btn-gris shadow">'
    + '<i class="bi bi-plus-square"></i>'
    + '</button>'
    + '</div>'
    downloadBtn = document.getElementById('download')
    console.log(downloadBtn)
    downloadBtn.addEventListener('click', function (event) {
      ipcRenderer.send('save-file-dialog')
    })
    Object.keys(data["Masculin"]).forEach(level => {
        document.getElementById("SelectLevel").innerHTML += "<option value=" + level + ">" +  level + "</option>"
  
    });
  }
})

if (btnSave){
  btnSave.addEventListener('click', function (event) {
    if(btnSave.id == "add"){
      const nb = arr => arr.map(Number);
      //console.log(Math.max.apply(Math, nb(Object.keys(data[s][l][t]))))
      if(l=="niveau3") idMax = Math.max.apply(Math, nb(Object.keys(data[s][l][t][c]))) + 1
      else idMax = Math.max.apply(Math, nb(Object.keys(data[s][l][t]))) + 1
      ipcRenderer.send('addJson', document.getElementById("modalApp").value, s, l, t, c, idMax)
    }
    else if(btnSave.id == "edit"){
      id = btnSave.value
      //console.log(btnSave.tagName)
      ipcRenderer.send('editJson', document.getElementById("modalApp").value, s, l, t, c, id)
    }
    else if(btnSave.id == "del"){
      id = btnSave.value
      ipcRenderer.send('delJson', s, l, t, c, id)
    }
  })
}

ipcRenderer.on('editDic', function (event) {
  var data = JSON.parse(fs.readFileSync(pathResolver.join( __dirname, "./Test3-Copie.json"), "utf8"));
  if(l=="niveau3") arrayType = data[s][l][t][c]
  else arrayType = data[s][l][t]
  html += "<table id='dicTable' class='table table-dark table-striped text-center shadow-lg'> <tr> <th> Appréciations </th> <th> Actions </th> </tr>"

  for (const key in arrayType) {
      html += "<tr>"
      html += "<td>" + arrayType[key] + "</td>"
      html += '<td class="col-1"><a onclick=edit(this.id) class="text-decoration-none" id='+key+' href="#"><i class="bi bi-pencil-square text-primary"></i> </a> <a id='+key+' onclick=del(this.id) class="text-decoration-none" href="#"><i class="bi bi-x-circle text-danger"></i></a></td>'
      html += "</tr>"
  }

  html += "</table>"
  document.getElementById("dicTable").innerHTML = html
  html = ""
})

ipcRenderer.on('saveFile', function (event, path, result) {

    const csvWriter = createCsvWriter({
      path: path,
      header: Object.keys(results[0]),
      fieldDelimiter : ";",
      encoding : "ascii"
    });
    console.log(path)
  
    saveTable = []
    saveRow = []
    test=[]
    var i = 0
    var y = 1
    tableRows = document.getElementById("csvTable").rows
    //var count = document.getElementById("csvTable").rows[0].cells.length
    Array.from(tableRows).forEach(function(row) {
      if(i!=0){
       
        Array.from(row.cells).forEach(function(cell) {
          console.log("y" + y)
          console.log(row.cells.length)
          
          if(y<=row.cells.length){
            console.log(cell.innerHTML)
            saveRow.push(cell.innerHTML)
          }
          
          y++
        })
        y = 1
        saveTable.push(saveRow)
        console.log(saveTable)
        saveRow = []
  
      }
      i++
    });
  
    if (process.platform == "darwin"){
      const {writeFile} = require('fs').promises
      const iconv = require('iconv-lite')
      const createCsvStringifier = require('csv-writer').createArrayCsvStringifier
  
      const toShiftJIS = text => iconv.encode(text, "macintosh")
      const csvStringifier = createCsvStringifier({
        header: Object.keys(results[0]),
        fieldDelimiter : ";"
      })
      const headerLine = csvStringifier.getHeaderString()
      const recordLines = csvStringifier.stringifyRecords(saveTable)
  
      writeFile(path, toShiftJIS(headerLine + recordLines))
      console.log("write file with darwin")
    }
    else{
      csvWriter.writeRecords(saveTable)       // returns a promise
      .then(() => {
          console.log('...Done');
      });
    }
})


//////////////////////////////////////////////////
/*PARAM*/

const btnSaveParam = document.getElementById("btnSaveParam")

if (btnSaveParam){
  btnSaveParam.addEventListener('click', function (event) {
    if(Number(document.getElementById("moySup").innerHTML) <= Number(document.getElementById("moyInf").innerHTML)){
      alert("erreur")
    }
    else {
      paramFile = './param.json';
      paramPos = document.getElementById("rangePos").value
      paramNeg = document.getElementById("rangeNeg").value
      ipcRenderer.send('save-param', paramFile, paramPos, paramNeg)
    }
  })
}
