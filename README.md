# Apprecia

## Pré Requis
* [NodeJs](https://nodejs.org/fr/download/)

## Lancement
* Importer le projet `git clone https://gitlab.com/BarHe974/apprecia.git`
* Entrer dans le dossier créé `cd apprecia`
* Executer la commande suivante `npm install`
* puis `npm start` pour lancer l'application (`npm install` est à executer qu'une seule fois)

## Attention
L'application n'est pas 100% terminée. Il pourrait y avoir des bugs et des soucis de compatibilités sur Mac car l'application a été développé sur Windows.
